#[derive(PartialEq, Debug, Clone, Copy)]
pub struct QLatin1Char {
    ch: u8,
}

impl From<u8> for QLatin1Char {
    fn from(ch: u8) -> QLatin1Char {
        QLatin1Char { ch }
    }
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct QChar {
    ucs: u16,
}

impl From<u16> for QChar {
    fn from(rc: u16) -> QChar {
        QChar { ucs: rc }
    }
}

impl From<char> for QChar {
    fn from(rc: char) -> QChar {
        QChar { ucs: rc as u16 }
    }
}

impl From<QLatin1Char> for QChar {
    fn from(ch: QLatin1Char) -> QChar {
        QChar { ucs: ch.ch as u16 }
    }
}

pub mod special_character {
    use super::QChar;
    pub const NULL: QChar = QChar { ucs: 0x0000 };
    pub const TABULATION: QChar = QChar { ucs: 0x0009 };
    pub const LINE_FEED: QChar = QChar { ucs: 0x000a };
    pub const CARRIAGE_RETURN: QChar = QChar { ucs: 0x000d };
    pub const SPACE: QChar = QChar { ucs: 0x0020 };
    pub const NBSP: QChar = QChar { ucs: 0x00a0 };
    pub const SOFT_HYPHEN: QChar = QChar { ucs: 0x00ad };
    pub const REPLACEMENT_CHARACTER: QChar = QChar { ucs: 0xfffd };
    pub const OBJECT_REPLACEMENT_CHARACTER: QChar = QChar { ucs: 0xfffc };
    pub const BYTE_ORDER_MARK: QChar = QChar { ucs: 0xfeff };
    pub const BYTE_ORDER_SWAPPED: QChar = QChar { ucs: 0xfffe };
    pub const PARAGRAPH_SEPARATOR: QChar = QChar { ucs: 0x2029 };
    pub const LINE_SEPARATOR: QChar = QChar { ucs: 0x2028 };
    // oops, this does not fit in u16
    // pub const LAST_VALID_CODE_POINT: QChar = QChar { ucs: 0x10ffff };
}
