use qtcore::qchar::special_character;
use qtcore::QChar;

/// This file contains the tests from
/// tests/auto/corelib/tools/qchar/tst_qchar.cpp

#[test]
fn from_char16_t() {
    let a_umlaut = QChar::from('\u{00E4}');
    assert_eq!(a_umlaut, QChar::from(0x00E4u16));
    let replacement_character = QChar::from('\u{FFFD}');
    assert_eq!(
        replacement_character,
        special_character::REPLACEMENT_CHARACTER
    );
}

/// tst_QChar::fromWchar_t is not relevant for rust

#[test]
fn operator_eqeq_null() {
    //    let a_umlaut = QChar::from(
}
